<?php

use yii\db\Migration;

class m170327_153214_create_category extends Migration
{
    public function safeUp()
    {
        
        $this->createTable('category', [
            'id'=>$this->primaryKey(),
            'name'=>$this->string(200)->notNull(),
            'status'=>$this->integer()->notNull(),
            'category_id'=>$this->integer()            
        ]);
        
        $this->addForeignKey('FK_category_category', 'category', 'category_id', 'category', 'id');
    }

    public function safeDown()
    {
        echo "m170327_153214_create_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170327_153214_create_category cannot be reverted.\n";

        return false;
    }
    */
}
