
    var article = {
        init: function(){
            $(document).ready(function(){
                $(".article_publish").on("click", function(event){
                    event.preventDefault();
                    $.ajax({ 
                        type: "POST", 
                        url: $(this).attr("href"), 
                        success: function(){
                            location.reload(); 
                        },
                        error: function(error){
                            console.log(error.responseText);
                        }
                    }); 
                });
            });
        }
    };