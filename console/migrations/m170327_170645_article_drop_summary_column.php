<?php

use yii\db\Migration;

class m170327_170645_article_drop_summary_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('article', 'summary');
    }

    public function safeDown()
    {
        echo "m170327_170645_article_drop_summary_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170327_170645_article_drop_summary_column cannot be reverted.\n";

        return false;
    }
    */
}
