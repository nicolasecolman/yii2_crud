<?php

use yii\db\Migration;

class m180417_124611_article_allow_comments_field extends Migration
{
    public function safeUp()
    {
        //Le paso un literal en vez de $this->tinyInteger() porque algunas versiones de yii2 no soportan dicho método
        //https://www.yiiframework.com/doc/api/2.0/yii-db-schemabuildertrait#tinyInteger()-detail
        //TODO: agregar un condicional por motor de base de datos, ya que tinyint solo existe para mysql
        $this->addColumn('article', 'allow_comments', 'TINYINT'); 
    }

    public function safeDown()
    {
        $this->dropColumn('article', 'allow_comments');
    }
}
