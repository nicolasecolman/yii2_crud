<?php

namespace frontend\controllers;

use Yii;
use app\models\Article;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'pageCache' => [
                'class' => 'yii\filters\PageCache',
                'only' => ['index', 'view'],
                'duration' => 30,
//                'dependency' => [
//                    'class' => 'yii\caching\DbDependency',
//                    'sql' => 'SELECT COUNT(*) FROM post',
//                ],
                'variations' => [
                    Yii::$app->request->get()
                    //Yii::$app->request->get('page') //Si quiero que no cachee cuando le llega el parametro page por GET
                ]
            ],
        ];
    }
    
    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Article::find(),
            'pagination' => [
                'pageSize' => 4
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);

    }
    
    /**
     * Ejemplo de como implementar cache dentro del mismo controlador
     * Esto es util cuando por algun motivo no podemos implementarlo dentro del behavior
     */
    public function actionIndex_()
    {
        //$start = microtime(true);
        $cachedContent = Yii::$app->cache->get('article-index');
        
        if(!$cachedContent){
        
            $dataProvider = new ActiveDataProvider([
                'query' => Article::find(),
                'pagination' => [
                    'pageSize' => 4
                ]
            ]);

            $cachedContent = $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);

            Yii::$app->cache->set('article-index', $cachedContent, 20);
        }/*else{
            echo "Hay contenido cacheado"; die;
        }*/
        
        //echo microtime(true) - $start;die;
        return $cachedContent;
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
