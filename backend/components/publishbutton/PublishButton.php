<?php

namespace app\components\publishbutton;

use yii\base\widget;
use yii\helpers\Html;

use backend\assets\PublishAsset;

class PublishButton extends \yii\base\Widget
{
    public $url;
    
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        
        PublishAsset::register($this->getView());
        $js = 'article.init()';
        $this->getView()->registerJs($js);

        return Html::a('Publish', $this->url, [
                            'title'=>'Publish',
                            'class'=>'article_publish'
                        ]);
    }
}
