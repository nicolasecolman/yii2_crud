<?php

namespace backend\modules\pictures\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * ArticlePictureSearch represents the model behind the search form about `backend\models\ArticlePicture`.
 */
class ArticlePictureSearch extends ArticlePicture
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'article_id'], 'integer'],
            [['description'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ArticlePicture::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
                
        $query->joinWith(['article'=>function($q){
            $q->alias('c');
        }]);
        
        $dataProvider->sort->attributes['article_id'] = [
            'asc' => ['c.title' => SORT_ASC],
            'desc' => ['c.title' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'article_picture.id' => $this->id,
            'article_picture.article_id' => $this->article_id,
        ]);

        $query->andFilterWhere(['like', 'article_picture.description', $this->description]);
        
        return $dataProvider;
    }
}
