<?php

namespace backend\components\web;

use yii\filters\VerbFilter;

/**
 * Description of Controller
 *
 * @author desarrollo-01
 */
class Controller extends \yii\web\Controller{
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'publish'=>['post'] //Solo permitimos llamadas a la accion "Publish" por método POST
                ],
            ],
            
            'ghost-access'=> [
                //'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
                'class' => \webvimark\modules\UserManagement\components\GhostAccessControl::className(),
            ],
        ];
    }
    
}
