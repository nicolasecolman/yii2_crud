<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'article_id',
            'title',
            'content:ntext',
            [
                'attribute'=>'status',
                'value'=>function($data){
                    return $data->getStatus($data->status);
                },
                'filter'=>$searchModel->getStatusList()
            ],
            [
                'attribute'=>'category_id',
                'value'=>function($data){
                    $names = "";
                    foreach($data->tCategories as $i => $category){                        
                        $url = '?r=article/index&ArticleSearch[category_id]=' . $category->id;
                        $names .= Html::a($category->name, $url) . (( $i < count($data->tCategories) - 1 )? ", " : "");
                    }
                    return $names;
                },
                'format'=>'raw',
                'filter'=>yii\helpers\ArrayHelper::map(\backend\models\Category::getForDropDown(), 'id', 'name'),
                'contentOptions'=>[
                    'style'=>'max-width: 100px; word-break: break-all; white-space: normal'
                ]
            ],
            //'create_time:datetime',
            ['class' => 'yii\grid\ActionColumn'],
            [
                'class'=>'yii\grid\ActionColumn', 
                'template'=>'{publish}', 
                'buttons'=>[
                    'publish'=>function($url, $model, $key){
                        //TODO: La validacion de estado esta bien pero habria que unificarla con la del modelo
                        /*return ($model->status != \backend\models\Article::STATUS_PUBLISHED)? Html::a('Publish', $url, ['title'=>'Publish','class'=>'article_publish']): "";*/

                        return 
                            ($model->status != \backend\models\Article::STATUS_PUBLISHED)? 
                                \app\components\publishbutton\PublishButton::widget(['url' => $url]): 
                                "";
                    }                    
                ]   
            ]
                        
        ],
    ]); ?>
</div>


<?php

    //$js = 'article.init()';
    //$this->registerJs($js);

?>
