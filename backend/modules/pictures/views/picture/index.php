<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Article Picture';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="article_picture-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'description',
            'article.title',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
