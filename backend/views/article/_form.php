<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use dosamigos\ckeditor\CKEditor;
use kartik\time\TimePicker;
use backend\models\Category;

/* @var $this yii\web\View */
/* @var $model backend\models\Article */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget( CKEditor::className(), 
        [
            'options'=>['rows' => 6],
            'preset'=>'basic'
        ]) ?>

    <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>

    <?= $form
            ->field($model, 'publish_time')
            ->widget(\yii\jui\DatePicker::classname(), 
            [
                'dateFormat'=>'dd-MM-yyyy', 
                'options'=>[
                    'class'=>'form-control'            
                ]
            ])
            ->label('Publish Date') ?>    
    
    <?= $form->field($model, 'publish_hour')->widget(TimePicker::classname(), []) ?>
        
    <div class="form-group" id="categories_container">
        <fieldset>
            <?php
                //Pjax::begin();

                    Modal::begin([                    
                        'header'=>'',
                        'id'=>'category-modal',
                        'toggleButton'=>[ 
                            'label'=>'', 
                            'id' => 'addCategory',
                            'value' => Url::to(['category/create']), 
                            'tag'=>'span',
                            'class'=>'glyphicon glyphicon-plus', 
                            'role'=>'button',
                        ]
                    ]);

                    echo "";

                    Modal::end();

                    $model->categories = [];

                    foreach($model->tCategories as $cat){
                        array_push($model->categories, $cat->id);
                    }

                    $fullList = Category::getFullList();
                    $list = ArrayHelper::map($fullList, 'id', 'name');

                    echo $form
                            ->field($model, 'categories')
                            ->label('Categories')
                            ->checkboxList(
                                $list,
                                [
                                    'separator'=>'<br>',
                                    'item'=>function($index, $label, $name, $checked, $value){

                                        $spaces = "";
                                        //$level = $fullList[$value];   //No puedo acceder desde aca

                                        $disable = false;
                                        $checkbox = Html::checkbox($name, $checked, ['value' => $value, 'disabled' => $disable]);
                                        return Html::label( $spaces .$checkbox . $label);
                                    }
                                ]
                            );

                    /*echo $form->field($model, 'categories')->hiddenInput(['value'=>'']);
                    echo "<ul>";
                    foreach($fullList as $cat)
                    {
                        $label = $cat["name"];
                        $checked = in_array($cat["id"], $model->categories);
                        $checkbox = Html::checkbox('Article[categories][]', $checked, ['value' =>$cat["id"]]);
                        echo Html::label($checkbox . $label, null, ["class"=>"col-xs-offset-" . $cat["level"] ]) . "<br>";

                        //Tira error "Array to string conversion" porque categories es un array
                        //echo $form->field($model, 'categories')->checkbox(['value'=>$cat["id"],'checked'=>$checked]);
                    }
                    echo "</ul>";           
                    */

                    echo '<div style="display:none;" ><a href="" id="update_categories" >Actualizar Lista</a>';
                //Pjax::end();
                
            ?>
        </fieldset>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>
    
    <script>
        var category = (function(){
            var instance = {};
            
            instance.init = function(){
                $(document).ready(function(){
                    
                    $(document).on('click', '#addCategory', function(event){
                        event.preventDefault();                        
                        $(".modal-body").load($(this).attr('value'));
                    });
                    
                    //$("#category-modal").on('hidden.bs.modal', function(){ //No usar esta sintaxis porque el modal puede ser pisado por pjax
                    $(document).on('hidden.bs.modal', "#category-modal", function(){
                        instance.reloadCategories();
                    });
                });
            }
            
            instance.reloadCategories = function(){
                var url = "<?php echo (isset($model->article_id))?Url::to(['article/categories', 'id'=>$model->article_id]): Url::to(['article/categories']); ?>";
                //Le agrego  #article-categories > * a la url para pisar el div contenedor
                $("#article-categories").load(url + " #article-categories > *"); 
                //$("#update_categories").trigger("click");
            }
            
            return instance;
        })();
        
    </script>
    
    <?php
        $js = 'category.init()';
        $this->registerJs($js);
    ?>
    
</div>
