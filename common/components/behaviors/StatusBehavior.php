<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components\behaviors;
use Yii;
use yii\base\Behavior;
use yii\base\InvalidCallException;
use yii\db\BaseActiveRecord;

class StatusBehavior extends yii\base\Behavior
{
    /* Un behavior q te agregue todo el comportamiento necesario para estados
    setStatus por ejemplo, te debería cambiar el estado a un nuevo estado sólo si es posible esa transición entre el estado anterior y el nuevo.
    En caso de no poder cambiar el estado por no pasar la validación, a nivel código te devolvería false por ejemplo, y ahí largas un error o haces algo al respecto     */
    
    public function setStatus($status)
    {
        
        if($this->validStatus('status', $status)){
            //Estado valido, hacer algo
            $this->owner->status = $status;
            return true;
        }else{
            return false;
        }

    }
    
    private function validStatus($attributeName, $status)
    {
        $valid = true;
        //Array de estados permitidos por estado
        $allowedTransitions = [
            \backend\models\Article::STATUS_DRAFT       => [\backend\models\Article::STATUS_PUBLISHED],
            \backend\models\Article::STATUS_PUBLISHED   => [\backend\models\Article::STATUS_UNPUBLISHED],
            \backend\models\Article::STATUS_UNPUBLISHED => [\backend\models\Article::STATUS_DRAFT, \backend\models\Article::STATUS_PUBLISHED]
        ];
        
        $statusList = $this->owner->statusList;
        $previous_status = $this->owner->previous_status;
        
        if($previous_status != $status)
        {
            
            if(isset($allowedTransitions[$previous_status]) && !in_array($status, $allowedTransitions[$previous_status]) )
            {
                $this->owner->addError($attributeName, 
                        "An article cannot pass from " 
                        . $statusList[$previous_status] 
                        . " to " 
                        . $statusList[$status]);
                $valid = false;
            }
        }
        
        return $valid;
                
    }
}
