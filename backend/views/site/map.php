<?php
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;

/**
 * ACLARACION: ES NECESARIO CONFIGURAR LA API-KEY EN main-local.php (NO CONVIENE EN main.php PARA EVITAR PUBLICAR LA API-KEY EN UN REPO
 * 'assetManager' => [
        'bundles' => [
            'dosamigos\google\maps\MapAsset' => [
                'options' => [
                    'key' => 'YOUR_API_KEY'
                ]
            ]
        ]
 */

//$coord = new LatLng(['lat' => 39.720089311812094, 'lng' => 2.91165944519042]);
//-32.863593153888075 -70.11474609375,-32.660342191881895 -69.41162109375
$coord = new LatLng(['lat'=>-32.863593153888075, 'lng'=>-70.11474609375]);
$map = new Map([
    'center' => $coord,
    'zoom' => 5,
    
    //Paso ancho del div contenedor por parámetro (tambien puede ser un nro en caso de querer medirlo en px)
    'width'=>'100%'
    //'height'=>'800px'//No sirve REVIEW
]);

// lets use the directions renderer
/*$home = new LatLng(['lat' => 39.720991014764536, 'lng' => 2.911801719665541]);
$school = new LatLng(['lat' => 39.719456079114956, 'lng' => 2.8979293346405166]);
$santo_domingo = new LatLng(['lat' => 39.72118906848983, 'lng' => 2.907628202438368]);

// setup just one waypoint (Google allows a max of 8)
$waypoints = [
    new DirectionsWayPoint(['location' => $santo_domingo])
];

$directionsRequest = new DirectionsRequest([
    'origin' => $home,
    'destination' => $school,
    'waypoints' => $waypoints,
    'travelMode' => TravelMode::DRIVING
]);

// Lets configure the polyline that renders the direction
$polylineOptions = new PolylineOptions([
    'strokeColor' => '#FFAA00',
    'draggable' => true
]);

// Now the renderer
$directionsRenderer = new DirectionsRenderer([
    'map' => $map->getName(),
    'polylineOptions' => $polylineOptions
]);

// Finally the directions service
$directionsService = new DirectionsService([
    'directionsRenderer' => $directionsRenderer,
    'directionsRequest' => $directionsRequest
]);

// Thats it, append the resulting script to the map
$map->appendScript($directionsService->getJs());

// Lets add a marker now
$marker = new Marker([
    'position' => $coord,
    'title' => 'My Home Town',
]);

// Provide a shared InfoWindow to the marker
$marker->attachInfoWindow(
    new InfoWindow([
        'content' => '<p>This is my super cool content</p>'
    ])
);

// Add marker to the map
$map->addOverlay($marker);
*/
// Now lets write a polygon
/*$coords = [
    new LatLng(['lat' => 25.774252, 'lng' => -80.190262]),
    new LatLng(['lat' => 18.466465, 'lng' => -66.118292]),
    new LatLng(['lat' => 32.321384, 'lng' => -64.75737]),
    new LatLng(['lat' => 25.774252, 'lng' => -80.190262])
];*/
$coords = [
        new LatLng(['lat' => -32.863593153888075, 'lng' => -70.11474609375]),
        new LatLng(['lat' => -32.660342191881895, 'lng' => -69.41162109375]),
        new LatLng(['lat' => -33.08479140401055, 'lng' => -68.9501953125]),
        new LatLng(['lat' => -32.88204749198677, 'lng' => -68.8623046875]),
        new LatLng(['lat' => -33.30543468003499, 'lng' => -66.3134765625]),
        new LatLng(['lat' => -35.731055268049104, 'lng' => -67.8955078125]),
        new LatLng(['lat' => -35.90922219847364, 'lng' => -70.3125]),
        new LatLng(['lat' => -35.58823407362938, 'lng' => -70.3564453125]),
        new LatLng(['lat' => -35.39143828911822, 'lng' => -70.4443359375])
    ];
        

$polygon = new Polygon([
    'paths' => $coords
]);

// Add a shared info window
$polygon->attachInfoWindow(new InfoWindow([
        'content' => '<p>This is my super cool Polygon</p>'
    ]));

// Add it now to the map
$map->addOverlay($polygon);

// Display the map -finally :)
echo $map->display();