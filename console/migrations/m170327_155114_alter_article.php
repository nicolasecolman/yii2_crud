<?php

use yii\db\Migration;

class m170327_155114_alter_article extends Migration
{
    public function safeUp()
    {
        $table = 'article';
        $this->addColumn($table, 'status', $this->smallInteger());
        $this->addColumn($table, 'create_time', $this->integer());
        $this->addColumn($table, 'update_time', $this->integer());
        $this->addColumn($table, 'publish_time', $this->integer());
    }

    public function safeDown()
    {
        echo "m170327_155114_alter_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170327_155114_alter_article cannot be reverted.\n";

        return false;
    }
    */
}
