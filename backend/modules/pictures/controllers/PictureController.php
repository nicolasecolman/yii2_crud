<?php

namespace backend\modules\pictures\controllers;

use Yii;
use backend\modules\pictures\models\ArticlePicture;
use backend\modules\pictures\models\ArticlePictureSearch;
use yii\data\ActiveDataProvider;
//use yii\web\Controller;
use backend\components\web\Controller;
use yii\web\UploadedFile;


class PictureController extends Controller{
    
    public function actionIndex()
    {
        
        $searchModel = new ArticlePictureSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel, 
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate()
    {
        $model = new \backend\modules\pictures\models\ArticlePicture();
        
        $model->load(Yii::$app->request->post()); //valido aca porque si lo hago antes de validate se valida erroneamente
        $model->files = UploadedFile::getInstances($model, 'files');
        
        if($model->validate()){
        
            foreach ($model->files as $file){

                $path = 'uploads/' . $file->baseName . '.' . $file->extension;

                $file->saveAs($path);

                $model->img = $path;
                $model->save(false);
                
                //Creo otra instancia y le seteo los atributos
                $model = new \backend\modules\pictures\models\ArticlePicture();        
                $model->load(Yii::$app->request->post());
            }
                        
            return $this->redirect(['index']);
        }else{
            return $this->render('create', [
                'model'=>$model
            ]);
        }
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        //Revisar primero create y despues hacer lo mismo aca
        
        if($model->load(Yii::$app->request->post()) && $model->save())
        {
            return $this->redirect(['index']);
        }else{
            return $this->render('update', [
                'model'=>$model
            ]);
        }
    }
    
    public function actionView($id)
    {
        return $this->render('view', [
            'model'=>$this->findModel($id)
        ]);
    }    
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ArticlePicture::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
