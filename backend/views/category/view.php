<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label'=>$model->getAttributeLabel('status'),
                'value'=>$model->getStatus($model->status)
            ],
            [
                'label'=>$model->getAttributeLabel('category_id'),
                'value'=>($model->category)? $model->category->name: ""
            ]
        ],
    ]) ?>

</div>

<hr>

<!--Pruebas con qrcode-library-->
<?php
//use Da\QrCode\QrCode;
$qrCode = (new \Da\QrCode\QrCode('This is my text........................................................'))
    ->setSize(250)
    ->setMargin(5)
    ->useForegroundColor(51, 153, 255);
echo '<img src="' . $qrCode->writeDataUri() . '">';
?>