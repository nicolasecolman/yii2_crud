<?php

namespace backend\controllers;

use Yii;
use backend\models\Article;
use backend\models\search\ArticleSearch;
use backend\components\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    protected $something = '';
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
        ]);
            //Pruebas
            /*'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //'only'=>...
                'rules'=>[
                    [
                        'allow'=>true,
                        'actions'=>['index', 'view', 'create', 'update', 'delete', 'publish'],
                        'roles'=>['?'] //Todos los roles, incluso usuarios no autenticados
                    ]
                ]
            ],*/
            /*
             * Cache del lado del cliente. No tiene sentido usarla en el backend
            'cache'=>[
                'class'=> \yii\filters\HttpCache::className(),
                //'only'=>['index'],
                'lastModified' => function ($action, $params) {
                    $q = new \yii\db\Query();                    
                    //return $q->from('article')->max('update_time');
                    return time();
                },
            ]*/
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->article_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->article_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionPublish($id)
    {
        $model = $this->findModel($id);
        
        if($model->setStatus(\backend\models\Article::STATUS_PUBLISHED)){
            $model->updateAttributes(['status']);
        }else{
            throw new \yii\web\HttpException(400, $model->getFirstError('status'));
        }
        
    }
    
    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
         
    public function actionCategories($id = null)
    {
        if(isset($id)){
            $model = $this->findModel($id);
        }else{
            $model = new Article();            
        }
        return $this->renderAjax('_categories', ['model' => $model]);
    }
}
