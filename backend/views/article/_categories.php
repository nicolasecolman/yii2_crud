<?php

    use yii\bootstrap\Modal;
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\widgets\Pjax;

    use backend\models\Category;
?>

<fieldset>
    <?php $form = new ActiveForm(); ?>
    <?php

        Modal::begin([                    
            'header'=>'',
            'toggleButton'=>[ 
                'label'=>'', 
                'id' => 'addCategory',
                'value' => Url::to(['category/create']), 
                'tag'=>'span',
                'class'=>'glyphicon glyphicon-plus', 
                'role'=>'button',
            ]
        ]);

        echo "";

        Modal::end();

        $model->categories = [];

        foreach($model->tCategories as $cat){
            array_push($model->categories, $cat->id);
        }

        $fullList = Category::getFullList();
        $list = ArrayHelper::map($fullList, 'id', 'name');

        echo $form
                ->field($model, 'categories')
                ->label('Categories')
                ->checkboxList(
                    $list,
                    [
                        'separator'=>'<br>',
                        'item'=>function($index, $label, $name, $checked, $value){

                            $spaces = "";
                            //$level = $fullList[$value];   //No puedo acceder desde aca

                            $disable = false;
                            $checkbox = Html::checkbox($name, $checked, ['value' => $value, 'disabled' => $disable]);
                            return Html::label( $spaces .$checkbox . $label);
                        }
                    ]
                );

    ?>
</fieldset>