<?php

use yii\db\Migration;

class m170308_193121_article_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('article', [
            'article_id' => $this->primaryKey(),
            'title' => $this->string(140),
            'summary' => $this->string(255),
            'content' => $this->text(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('article');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
