<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Article;

/**
 * ArticleSearch represents the model behind the search form about `backend\models\Article`.
 */
class ArticleSearch extends Article
{
    public $category_id;
    //public $cacheControlHeader = 'public, max-age=3600000';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'status', 'create_time', 'update_time', 'publish_time'], 'integer'],
            [['title', 'content'], 'safe'],
            ['category_id', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->getQuery($params);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'article_id' => SORT_DESC
                ]
            ],
            /*'sort'=>[
                'attributes'=>[
                    'article_id',
                    'title',
                    'content',
                    'status',
                    'tCategories'
                ]
            ],*/
            'pagination'=>[
                'pageSize'=>5
            ]
        ]);
        
        /*$dataProvider->sort->attributes["tCategories"] = [
            'asc'=>['category.name'=>SORT_ASC],
            'desc'=>['category.name'=>SORT_DESC]
        ];*/
        
        //Traigo cantidad total de articulos (si no pongo esto me trae la cantidad total de article_category
        $countQuery = clone ($query);
        $dataProvider->totalCount = $countQuery->count();
        
        return $dataProvider;
    }
    
    private function getQuery($params, $lazy = false)
    {
        $query = Article::find();
        
        if(!$lazy){
            $query->with(['tCategories']);
        }
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        //TRAE PROBLEMAS CON LA CANTIDAD DE ITEMS DEL PAGINADOR PERO SI NO LO USO NO PUEDO FILTRAR POR CATEGORY_ID
        //SOLUCION: OBTENER EL TOTAL Y ASIGNARSELO AL TOTALCOUNT DEL DATAPROVIDER
        if($this->category_id){
            $query->joinWith(['tCategories' => function($query){
                $query->andFilterWhere(['category.id' => $this->category_id]);
            }]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'article.article_id' => $this->article_id,
            'article.status' => $this->status,
            'article.create_time' => $this->create_time,
            'article.update_time' => $this->update_time,
            'article.publish_time' => $this->publish_time,
        ]);

        $query->andFilterWhere(['like', 'article.title', $this->title])
            ->andFilterWhere(['like', 'article.content', $this->content]);
        
        return $query;
        
    }
}
