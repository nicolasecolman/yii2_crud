<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->article_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->article_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'article_id',
            'title',
            'content:ntext',
            [
              'label'=>$model->getAttributeLabel('status'),
              'value'=>$model->getStatus($model->status)
            ],
            'create_time:datetime',
            'update_time:datetime',
            'publish_time:datetime',
            [
                'label'=>'Categories',
                'value'=>function($model)
                {
                    $cats = "";
                    foreach($model->tCategories as $i => $cat)
                    {
                        
                        $cats.= $cat->name . ( ( $i < count($model->tCategories) - 1 )? ", ": "" ) ;
                    }
                    return $cats;
                }
            ]
        ],
    ]) ?>

</div>
