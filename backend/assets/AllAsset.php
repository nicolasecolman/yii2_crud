<?php
namespace backend\assets;

use yii\web\AssetBundle;
/**
 * Description of AllAsset
 *
 * @author 
 */
class AllAsset extends AssetBundle{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        
    ];
    public $js = [
        'js/all-a8451393c47c78452767561cbaeb246d',
    ];
    public $depends = [
        //'yii\web\JqueryAsset',
    ];
}
