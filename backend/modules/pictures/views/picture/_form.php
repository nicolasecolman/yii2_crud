<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(['options'=>[ 'enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'article_id')->dropDownList(yii\helpers\ArrayHelper::map(backend\modules\pictures\models\Article::find()->all(), 'article_id', 'title'), ['prompt'=>'Choose an article']) ?>
    
    <?php 
        /*echo $form
            ->field($model, 'files[]')
            ->widget(kartik\file\FileInput::className(),             
            [
                'options' => [
                    'accept' => 'image/*', 
                    'multiple' => true
                ], 
            ]); */
        
        echo $form->field($model, 'files[]')->fileInput();
        echo $form->field($model, 'files[]')->fileInput();
        echo $form->field($model, 'files[]')->fileInput();
    ?>
    
    <?= $form->field($model, 'description')->textarea() ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
