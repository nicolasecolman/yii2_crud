<?php
namespace common\fixtures;

use yii\test\ActiveFixture;
/**
 * Para cargar fixture ir al raiz del proyecto y hacer:
 * ./yii fixture/load "*"
 */
class UserFixture extends ActiveFixture
{
    public $modelClass = '\common\models\User';
    //public $dataFile = __DIR__.'/data/user.php';    
}