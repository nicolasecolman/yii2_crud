<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $category_id
 *
 * @property ArticleCategory[] $articleCategories
 * @property Category $category
 * @property Category[] $categories
 */
class Category extends \yii\db\ActiveRecord
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 2;
    
    private $statusList = [
        self::STATUS_ENABLED=>"Enabled",
        self::STATUS_DISABLED=>"Disabled"
    ];
    
    //private $list = [];
    //private $fullList = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status', 'category_id'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'category_id' => 'Parent Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategories()
    {
        return $this->hasMany(ArticleCategory::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['category_id' => 'id']);
    }
    
    //Funciones propias
    public function getStatusList()
    {
        return $this->statusList;
    }
    
    public function getStatus($code)
    {
        return (isset($this->statusList[$code]))? $this->statusList[$code]: null;
    }
    
    public static function getForDropDown($id = null)
    {
        $nodes = null;
        if(isset($id)){
            $nodes = self::find()->where("id!=" . $id)->orderBy('name')->all();
        }else{
            $nodes = self::find()->orderBy('name')->all();
        }
        
        $tree = self::getTree($nodes);

        $list = [];

        self::loadTreeItems($tree, 0, $list);

        return $list;
    }
    
    /*
     * Devuelve una lista anidada de categorías a partir de una lista devuelta por ActiveRecord
     */
    private function getTree($nodes) 
    {
        $refs = array();
        $list = array();

        foreach ($nodes as $data) {
            $thisref = &$refs[ $data['id'] ];
            $thisref['category_id'] = $data['category_id'];
            $thisref['text'] = $data['name'];
            if ($data['category_id'] == 0) {
                $list[ $data['id'] ] = &$thisref;
            } else {
                $refs[ $data['category_id'] ]['children'][ $data['id'] ] = &$thisref;
            }   
        }           
        return $list;
    }

    /*
     * Funcion recursiva para cargar una array plano de categorías con el formato: array("id"=>$key, "name"=>$name)
     * @tree: lista de listas 
     * @level: entero que indica el nivel de jerarquía
     * @list: array por referencia
     */
    private function loadTreeItems($tree, $level, &$list)
    {
        foreach($tree as $key => $item)
        {

            $name = $item["text"];
            for($i = 0; $i < $level; $i++)
            {
                $name = "-" . $name;
            }

            array_push($list, array("id"=>$key, "name"=>$name));

            if(isset($item["children"]))
            {
                $nextlevel = $level + 1;
                self::loadTreeItems($item["children"], $nextlevel, $list);
            }

        }
    }    

    /*
     * Función estática recursiva que carga una lista con todas las categorias y un campo level que es el nivel de anidamiento
     */
    private static function loadFullList($tree, $level, &$fullList)
    {
        foreach($tree as $key => $item)
        {

            array_push($fullList, array("id"=>$key, "name"=>$item["text"], "level"=>$level));

            if(isset($item["children"]))
            {
                $nextlevel = $level + 1;
                self::loadFullList($item["children"], $nextlevel, $fullList);
            }

        }
    }

    /*
     * Devuelve lista de categorias con nivel de anidamiento
     * Ya no uso una propiedad del objeto para poder llamar a esta funcion desde una vista sin tener que crear una instancia del objeto
     */
    public static function getFullList()
    {
        $fullList = [];

        self::loadFullList(self::getTree( self::find()->orderBy('name')->all() ), 0, $fullList);

        return $fullList;
    }
    
}
