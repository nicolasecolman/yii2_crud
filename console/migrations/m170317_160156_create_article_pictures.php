<?php

use yii\db\Migration;

class m170317_160156_create_article_pictures extends Migration
{
    
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('article_picture', [
            'id'=>$this->primaryKey(),
            'article_id'=>$this->integer()->notNull(),
            'img'=>$this->binary()->notNull(),
            'description'=>$this->text()
        ]);
        
        $this->addForeignKey('FK_article_picture_article', 'article_picture', 'article_id', 'article', 'article_id');
    }

    public function safeDown()
    {
        $this->dropTable('article_picture');
    }
    
}
