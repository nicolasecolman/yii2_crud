<?php
namespace console\controllers;
use yii\console\Controller;
use backend\models\Category;
/*
 * Clase de prueba para aprender a codear métodos invocables desde la consola
 * Ejemplo: 
 * - Para llamar al método index desde la consola, pararse en el raiz de la app y escribir:
 * ./yii consola/index  
 * - Para llamar al método index desde una tarea cron cada un minuto, escribir esta línea dentro de la lista de tareas:
 * * * * * * /opt/lampp/htdocs/yii2_crud/yii consola/index
 */
class ConsolaController extends Controller{
    
    public function actionIndex()
    {
        
        $model = new Category();
        $model->name = 'Una de prueba';
        $model->status = 10;
        /*if($model->save())
            echo "Dentro del action index!!\n";
        else
            var_dump(\yii\bootstrap\ActiveForm::validate($model));*/
    }
}
