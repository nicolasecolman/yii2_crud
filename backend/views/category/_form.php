<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin([
        'id'=>'category_form', 
        'enableClientValidation'=>true, //Descomentar esta linea para probar validacion del lado del servidor
        'enableAjaxValidation'=>false,
        'validationUrl'=>Url::to(['category/validate'])
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>
    
    <?= $form->field($model, 'category_id')->dropDownList(yii\helpers\ArrayHelper::map($model->getForDropDown($model->id), 'id', 'name'), ['prompt'=>'Choose a category']) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>

var category_form = (function(){
    var instance = {}
    
    instance.init = function(){
        $(document).ready(function(){
            $('#category_form')
            .on('beforeSubmit', function(event){
                
                event.preventDefault(); //Evito que haga dos llamadas AJAX si tengo habilitada la validación por AJAX
        
                var form = $(this);
                var formData = form.serialize();
                $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: formData,
                    success: function(data){ 
                        console.log("Callback");
                        console.log(data);
                        if(data.valid){
                            //Cierro modal - TODO: buscar la forma mas correcta de hacerlo
                            //$("#addCategory").trigger('click');
                            //Otras forma
                            $("#category-modal").modal('hide');
                            toastr.success('La categoría se guardó correctamente', 'Alta categoría');
                        }else{
                            $('#category_form').yiiActiveForm('updateMessages', data.errors, true);
                        }
                        
                    },
                    error: function(error){
                        console.log(error);
                        toastr.error('Hubo un error al intentar guardar la categoría', 'Alta categoría');
                    }
                });
                
                return false;
            })
            .on('submit', function(event){
                event.preventDefault();
            })
        })
    };
    
    return instance;
})();
</script>

<?php
    if(Yii::$app->request->isAjax){
        $js = 'category_form.init()';
        $this->registerJs($js);
    }
?>
