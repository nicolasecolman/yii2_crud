<?php

use yii\db\Migration;

class m170327_155840_create_article_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('article_category', [
            'article_id'=>$this->integer()->notNull(),
            'category_id'=>$this->integer()->notNull()
        ]);
        
        $this->addPrimaryKey('PK_article_category', 'article_category', ['article_id', 'category_id']);
        $this->addForeignKey('FK_article_category_article', 'article_category', 'article_id', 'article', 'article_id');
        $this->addForeignKey('FK_article_category_category', 'article_category', 'category_id', 'category', 'id');
        
    }

    public function safeDown()
    {
        $this->dropTable('article_category');
        return true;
        
        //echo "m170327_155840_create_article_category cannot be reverted.\n";
        //return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170327_155840_create_article_category cannot be reverted.\n";

        return false;
    }
    */
}
