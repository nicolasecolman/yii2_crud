<?php

namespace backend\assets;

use yii\web\AssetBundle;

class PublishAsset extends \yii\web\AssetBundle
{
    //public $sourcePath = '@vendor/nicolas/yii2-publishbutton';
    //public $basePath = '@webroot';
    public $sourcePath = '@app/components/publishbutton/';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        'assets/js/publish.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public $jsOptions = [
        //'position'=>\yii\web\View::POS_HEAD, //por defecto es POS_END
        //'condition'=>'lte IE9'
    ];
    
    public function init()
    {
        parent::init();
        $this->publishOptions['forceCopy'] = true;
    }
}