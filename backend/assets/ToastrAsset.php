<?php
namespace backend\assets;

use yii\web\AssetBundle;
/**
 * Description of ToastrAsset
 *
 * @author 
 */
class ToastrAsset extends AssetBundle{
    public $sourcePath = '@bower/toastr';
    public $css = [
        'toastr.min.css',
    ];
    public $js = [
        'toastr.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
