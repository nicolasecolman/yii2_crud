<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "article".
 *
 * @property integer $article_id
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property integer $create_time
 * @property integer $update_time
 * @property integer $publish_time
 *
 * @property ArticleCategory[] $articleCategories
 */
class Article extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_PUBLISHED = 2;
    const STATUS_UNPUBLISHED = 3;
    
    public static $statusList = [
        self::STATUS_DRAFT=>"Draft",
        self::STATUS_PUBLISHED=>"Published",
        self::STATUS_UNPUBLISHED=>"Unpublished"
    ];
    
    public $previous_status;
    public $publish_hour;  
    public $categories;  
    
    public function behaviors()
    {
        
        return [
            [
                'class'=> \yii\behaviors\TimestampBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_BEFORE_INSERT=>['create_time']
                ]
            ],
            [
                'class'=> \common\components\behaviors\StatusBehavior::className()
            ]
        ];
    }
    
    public function getStatusList()
    {
        return self::$statusList;
    }
    
    public function getStatus($code = null)
    {
        return (isset($this->statusList[$code]))? $this->statusList[$code]: null;
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['content'], 'string'],
            [['status', 'create_time', 'update_time'], 'integer'], 
            //Si saco esto no guarda el campo fecha de publicación
            //Esto es porque no se copiaría en el modelo durante la asignación masiva de atributos
            [['publish_time', 'publish_hour'], 'safe'], 
            [['title'], 'string', 'max' => 140],
            //['status', 'validStatus'],
            ['categories', 'safe'],
            ['categories', 'required'],
            //['categories', 'arrayWithAtLeastOneElement'] //No hace falta, ya lo valida "required". En el form de yii2 usé checkBoxList pero en el de yii use checkBox
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'article_id' => Yii::t('app', 'Article ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'status' => Yii::t('app', 'Status'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'publish_time' => Yii::t('app', 'Publish Date'),
            'publish_hour'=>Yii::t('app', 'Publish Time'),
            'tCategories'=>Yii::t('app', 'Categories')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleCategories()
    {
        return $this->hasMany(ArticleCategory::className(), ['article_id' => 'article_id']);
    }    
    
    public function getTCategories()
    {
        
        return $this
                ->hasMany(Category::className(), ['id' => 'category_id'])
                ->viaTable('article_category', ['article_id' => 'article_id'])
                ->orderBy(['category.name'=>SORT_ASC]);
    }
    
    public function beforeSave($insert)
    {
    
        if (parent::beforeSave($insert)) {
            
            //Cambio siempre la fecha de edicion
            $this->update_time = time();

            //Antes de guardar le cambio el formato a la fecha por integer si llega algo. NO USAR $_POST ACA!!!!!!!
            //TODO: controlar si viene definida para que no quede como 1 del 1 de 1970
            //if(isset($this->publish_time)){
                $this->publish_time = strtotime($this->publish_time . $this->publish_hour);
            //}
            
            //Hago el cambio de estado con un metodo inyectado por el behavior StatusBehavior
            if(!$this->setStatus($this->status))
            {
                return false;
            }
                
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * Elimina la relación con categorías
     * Esto es util antes de actualizar para evitar duplicados y antes de borrar para evitar el error por la constraint
     * TODO: investigar uso de "unlink"
     */
    private function deleteRelatedCategories()
    {
        foreach($this->articleCategories as $cat){
            $cat->delete();
        }
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        
        $this->deleteRelatedCategories();
        
        foreach ($this->categories as $cat){
            $articleCategory = new ArticleCategory();
            $articleCategory->category_id = $cat;
            
            $this->link('articleCategories', $articleCategory); /*Same as: $articleCategory->article_id = $this->article_id;  $articleCategory->save();*/
        }
        
        parent::afterSave($insert, $changedAttributes);
    }
    
    public function afterFind() 
    {
        
        //$this->publish_time = Yii::$app->formatter->asDate($this->publish_time);
        $this->previous_status = $this->status;
        $this->publish_hour = date("H:i", $this->publish_time);
        parent::afterFind();
    }
    
    public function beforeDelete()
    {
        $this->deleteRelatedCategories();
        parent::beforeDelete();
        return true;
    }
        
    /*
    * El artículo podrá pasar de estado draft a published y de estado published a unpublished. Desde el estado unpublished, podrá pasar a cualquiera de los otros estados.
    * Ya no uso mas esta validación aca. Valido desde un behavior
    */
    /*public function validStatus($attributeName)
    {
        
        //Array de estados permitidos por estado
        $vs = [
            self::STATUS_DRAFT =>[self::STATUS_PUBLISHED],
            self::STATUS_PUBLISHED=>[self::STATUS_UNPUBLISHED],
            self::STATUS_UNPUBLISHED=>[self::STATUS_DRAFT, self::STATUS_PUBLISHED]
        ];
        
        if($this->previous_status != $this->status)
        {
            if(isset($vs[$this->previous_status]) && !in_array($this->status, $vs[$this->previous_status]) )
            {
                $this->addError($attributeName, "An article cannot pass from " . $this->statusList[$this->previous_status] . " to " . $this->statusList[$this->status]);
            }
        }
    }*/
    
    /*
    public function arrayWithAtLeastOneElement($attributeName)
    {
        var_dump($this->categories); die;
       if(count($this->categories) == 0){
           $this->addError($attributeName, "An article must have at least one category");
       }
    }*/
}
