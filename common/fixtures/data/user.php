<?php
//CUIDADO!! al correr este fixture se elimina el usuario administrador
return [
    [
        'username' => 'bayer.hudson',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR',
        'password_hash' => '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
        'created_at' => '1402312317',
        'updated_at' => '1402312317',
        'email' => 'nicole.paucek@schultz.info',
    ],    
    [
        'username' => 'nicole.cubero',
        'auth_key' => 'HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR',
        'password_hash' => '$2y$13$EjaPFBnZOQsHdGuHI.xvhuDp1fHpo8hKRSk6yshqa9c5EG8s3C3lO',
        'created_at' => '1402312317',
        'updated_at' => '1402312317',
        'email' => 'nicole.cubero@gmail.com',
    ],
];
