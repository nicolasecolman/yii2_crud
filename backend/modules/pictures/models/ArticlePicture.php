<?php

namespace backend\modules\pictures\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "article_picture".
 *
 * @property integer $id
 * @property integer $article_id
 * @property resource $img
 * @property string $description
 *
 * @property Article $article
 */
class ArticlePicture extends \yii\db\ActiveRecord
{
    
    public $files = [];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article_picture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id'], 'required'],
            [['article_id'], 'integer'],
            [['description'], 'string'],
            //[['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'article_id']],
            [['files'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif', 'maxFiles' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'article_id' => Yii::t('app', 'Article'),
            'img' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['article_id' => 'article_id']);
    }
}
