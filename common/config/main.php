<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ]
        //No me toma esta configuracion porque toma la de main-local.php
        /*,
        'db'=>[
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=crud',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ]*/
        
    ],
];
